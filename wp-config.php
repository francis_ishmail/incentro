<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'incentro' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ':#u?Xh~`D.nY:.2<URu;5;D5T;q|)Us95p?b5>(lc7J(KX-%c`!@x%8^Jzfnwzf3' );
define( 'SECURE_AUTH_KEY',  'API*IVs$-heW%9+D(^So%ja63IQ$F0;$rqVrTc(!G92e <x he ^f23mEQpb}~(9' );
define( 'LOGGED_IN_KEY',    '2MA?p(}y3xiRV<rrHPKwbfJDEu),x`Y+`E)}v3IM)k!QvT&&+,}iU$j:UcQqluYh' );
define( 'NONCE_KEY',        '&S-6g/o-~8IYwRG4:WjD-Q,Q@pWcgQwu[/L~QVK<<%RT97X=jo8~GItx]HvBOrj$' );
define( 'AUTH_SALT',        '1x~*/=9r[Me>*j)e%)fC=9@-viv{U^PjjaoD#=m@*&@A^f|/+zR 8QzKS=wRyi0c' );
define( 'SECURE_AUTH_SALT', ' Qe=#u^*?dgI<BU_aTew9QBRSlk1=N=4SEy{iDi7_a+HV9#4M==U{L;55Te0p]<a' );
define( 'LOGGED_IN_SALT',   ':K9tO`=Z!o&stw{rAp1b7GW ,3VnF*6xtx4xHX}rtqoa>D{aEPQW#qq67/2@b@*w' );
define( 'NONCE_SALT',       'GsZdP#!:@Wdv;z4srPDMmTCBfM0gk&3%+0u3)64fm[WbD/}pX?HGFl(8M}fkGg4o' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
